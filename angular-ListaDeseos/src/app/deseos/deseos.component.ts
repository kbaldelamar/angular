import { Component, OnInit , Input, HostBinding} from '@angular/core';
import {atributosDeseo } from  './../model/deseos.model.';

@Component({
  selector: 'app-deseos',
  templateUrl: './deseos.component.html',
  styleUrls: ['./deseos.component.css']
})
export class DeseosComponent implements OnInit {
  @Input()
   destinos!: atributosDeseo;

  @HostBinding('attr.class') ccsClass='col-md-4'; 

  constructor() { }

  ngOnInit(): void {
  }

}
