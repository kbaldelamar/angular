import { Component, OnInit , Input} from '@angular/core';
import {atributosDeseo } from  './../model/deseos.model.';

@Component({
  selector: 'app-agregar-deseos',
  templateUrl: './agregar-deseos.component.html',
  styleUrls: ['./agregar-deseos.component.css']
})
export class AgregarDeseosComponent implements OnInit {
  
  public destinos:atributosDeseo[]=[];
  constructor() { }

  ngOnInit(): void {
  }

  guardar(nombres:string , apellidos:string, deseo:string):boolean{
    
    console.log(this.destinos)
     this.destinos.push(new atributosDeseo(nombres ,apellidos, deseo ) );
     
     return false;
  }

}
