import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarDeseosComponent } from './agregar-deseos.component';

describe('AgregarDeseosComponent', () => {
  let component: AgregarDeseosComponent;
  let fixture: ComponentFixture<AgregarDeseosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarDeseosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarDeseosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
