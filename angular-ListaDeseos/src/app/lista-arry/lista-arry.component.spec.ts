import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaArryComponent } from './lista-arry.component';

describe('ListaArryComponent', () => {
  let component: ListaArryComponent;
  let fixture: ComponentFixture<ListaArryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaArryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaArryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
