import { Component, OnInit  } from '@angular/core';

@Component({
  selector: 'app-lista-arry',
  templateUrl: './lista-arry.component.html',
  styleUrls: ['./lista-arry.component.css']
})
export class ListaArryComponent implements OnInit {
  listado:string[];
  constructor() { 
    this.listado=["kevin","angelica","manuel"];

  }

  ngOnInit(): void {
  }

}
