import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mensaje-bienvenida',
  templateUrl: './mensaje-bienvenida.component.html',
  styleUrls: ['./mensaje-bienvenida.component.css']
})
export class MensajeBienvenidaComponent implements OnInit {
   titulo:string;
  constructor() { 
  this.titulo="Bienvenido a tu lista de deseos";
  }

  ngOnInit(): void {
  }

}
