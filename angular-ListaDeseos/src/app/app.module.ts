import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MensajeBienvenidaComponent } from './mensaje-bienvenida/mensaje-bienvenida.component';
import { ListaArryComponent } from './lista-arry/lista-arry.component';
import { AgregarDeseosComponent } from './agregar-deseos/agregar-deseos.component';
import { TablaDeseosComponent } from './tabla-deseos/tabla-deseos.component';
import { DeseosComponent } from './deseos/deseos.component';

@NgModule({
  declarations: [
    AppComponent,
    MensajeBienvenidaComponent,
    ListaArryComponent,
    AgregarDeseosComponent,
    TablaDeseosComponent,
    DeseosComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
