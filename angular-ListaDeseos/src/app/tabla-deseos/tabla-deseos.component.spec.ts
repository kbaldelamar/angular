import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaDeseosComponent } from './tabla-deseos.component';

describe('TablaDeseosComponent', () => {
  let component: TablaDeseosComponent;
  let fixture: ComponentFixture<TablaDeseosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaDeseosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaDeseosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
